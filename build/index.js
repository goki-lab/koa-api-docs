"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _handlebars = _interopRequireDefault(require("handlebars"));

var _joiParser = _interopRequireDefault(require("./joiParser"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var isRouter = function isRouter(item) {
  return item.constructor.name === 'Router';
};

var unique = function unique(value, index, self) {
  return self.indexOf(value) === index;
}; /// ### docs
/// This is a handler which will create documentation of the routes
/// using Handlebars templating system
///
/// Templates are defined in template folder


var docs = function docs(routes) {
  _handlebars["default"].registerHelper('parseJoi', function (context, options) {
    var stack = context.filter(function (i) {
      return i.schema;
    });
    if (stack.length === 0) return '';
    return options.fn(stack[0].schema);
  });

  _handlebars["default"].registerHelper('isNotEmpty', function (context, options) {
    if (Array.isArray(context) && context.length > 0) {
      var uniq = context.filter(unique);

      if (uniq.length > 1 || uniq[0] !== '') {
        return options.fn(context);
      }
    }
  });

  _handlebars["default"].registerHelper('joiOrNot', function (conditional, options) {
    if (conditional && conditional.isJoi) {
      return options.fn(conditional);
    } else {
      return options.inverse(conditional);
    }
  });

  _handlebars["default"].registerHelper('describe', function (context, options) {
    if (context && context.isJoi) context = (0, _joiParser["default"])(context);
    return options.fn(new _handlebars["default"].SafeString(context));
  });

  _handlebars["default"].registerHelper('showit', function (context) {
    console.log('::SHOW::::::::', context);
    return '';
  });

  var partials = ['header', 'style', 'script', 'footer', 'validations'];
  partials.forEach(function (partial) {
    _handlebars["default"].registerPartial(partial, _fs["default"].readFileSync(_path["default"].join(__dirname, "../template/".concat(partial, ".handlebars")), 'utf8'));
  });

  var template = _handlebars["default"].compile(_fs["default"].readFileSync(_path["default"].join(__dirname, '../template/list.handlebars'), 'utf8'));

  return template({
    routes: routes
  });
}; /// ### KoaAPIDocs
/// This is Koa middleware to add Joi documentation on top of the routes.
/// It needs to get list of routes and Joi validation schema should be on it's middleware as a property `valiationMiddleware.schema = JoiSchema`
///
/// There are few parameters you can configure `app.user(routes, {url: string, hideAt: array, enabled: boolean} )`
/// `url`: By default the documentation will be available on `/docs` url
/// `hideAt`: By default if process.env.NODE_ENV is one of these ['production'], the middleware will bypassed
/// `enabled`: by default the value is true and it is enabled.


var KoaAPIDocs = function KoaAPIDocs(routes) {
  var configs = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var defaults = {
    url: '/docs',
    hideAt: ['production'],
    enabled: true
  };
  configs = Object.assign(defaults, configs);
  var error = null;

  try {
    var isSingleRouter = isRouter(routes);
    var isArray = Array.isArray(routes);

    if (isArray) {
      routes.forEach(function (route) {
        if (!isRouter(route)) throw 'You should pass a koa router or array of koa routers';
      });
    }

    if (!isSingleRouter && !isArray) throw 'You should pass a koa router or array of koa routers';
    routes = isSingleRouter ? [routes] : routes.flat();
  } catch (err) {
    error = err;
  }

  return function (ctx, next) {
    if (!configs.enabled || configs.hideAt.indexOf(process.env.NODE_ENV) > -1 || ctx.url !== configs.url) {
      return next();
    }

    ctx.body = error ? "error: ".concat(error.message) : docs(routes);
  };
};

var _default = KoaAPIDocs;
exports["default"] = _default;