import fs from 'fs'
import path from 'path'
import Handlebars from 'handlebars'
import JoiParser from './joiParser'

const isRouter = (item: any) => (item.constructor.name === 'Router')

const unique = (value, index, self) => {
  return self.indexOf(value) === index
}

/// ### docs
/// This is a handler which will create documentation of the routes
/// using Handlebars templating system
///
/// Templates are defined in template folder
const docs = (routes: any) => {
  Handlebars.registerHelper('parseJoi', (context, options) => {
    const stack = context.filter((i) => i.schema)
    if (stack.length === 0 ) return ''
    return options.fn(stack[0].schema)
  })
  Handlebars.registerHelper('isNotEmpty', (context, options) => {
    if (Array.isArray(context) && context.length > 0 ) {
      const uniq = context.filter(unique)
      if (uniq.length > 1 || uniq[0] !== '' ) {
        return options.fn(context)
      }
    }
  })
  Handlebars.registerHelper('joiOrNot', (conditional, options) => {
    if (conditional && conditional.isJoi) {
      return options.fn(conditional)
    } else {
      return options.inverse(conditional)
    }
  })
  Handlebars.registerHelper('describe', (context, options) => {
    if (context && context.isJoi) context = JoiParser(context)
    return options.fn(new Handlebars.SafeString(context))
  })
  Handlebars.registerHelper('showit', (context) => {
    console.log('::SHOW::::::::', context)
    return ''
  })

  const partials = [ 'header', 'style', 'script', 'footer', 'validations']
  partials.forEach((partial) => {
    Handlebars.registerPartial(partial, fs.readFileSync(path.join(__dirname, `../template/${partial}.handlebars`), 'utf8'))
  })
  const template = Handlebars.compile(fs.readFileSync(path.join(__dirname, '../template/list.handlebars'), 'utf8'))
  return template({ routes })
}

/// ### KoaAPIDocs
/// This is Koa middleware to add Joi documentation on top of the routes.
/// It needs to get list of routes and Joi validation schema should be on it's middleware as a property `valiationMiddleware.schema = JoiSchema`
///
/// There are few parameters you can configure `app.user(routes, {url: string, hideAt: array, enabled: boolean} )`
/// `url`: By default the documentation will be available on `/docs` url
/// `hideAt`: By default if process.env.NODE_ENV is one of these ['production'], the middleware will bypassed
/// `enabled`: by default the value is true and it is enabled.
const KoaAPIDocs = (routes, configs = {}) => {
  const defaults = {
    url: '/docs',
    hideAt: ['production'],
    enabled: true
  }
  configs = Object.assign(defaults, configs)

  let error = null
  try {
    const isSingleRouter = isRouter(routes)
    const isArray = Array.isArray(routes)
    if (isArray) {
      routes.forEach((route: any) => {
        if (!isRouter(route)) throw 'You should pass a koa router or array of koa routers'
      })
    }
    if (!isSingleRouter && !isArray) throw 'You should pass a koa router or array of koa routers'
    routes = isSingleRouter ? [routes] : routes.flat()
  } catch (err) {
    error = err
  }
  return (ctx, next) => {
    if (!configs.enabled || configs.hideAt.indexOf(process.env.NODE_ENV) > -1 || ctx.url !== configs.url) {
      return next()
    }
    ctx.body = error ? `error: ${error.message}` : docs(routes)
  }
}

export default KoaAPIDocs