"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = __importDefault(require("@koa/router"));
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var handlebars_1 = __importDefault(require("handlebars"));
var joiParser_1 = __importDefault(require("./joiParser"));
var router = new router_1.default();
var isRouter = function (item) { return (item.constructor.name === 'Router'); };
var unique = function (value, index, self) {
    return self.indexOf(value) === index;
};
/// ### docs
/// This is a handler which will create documentation of the routes
/// using Handlebars templating system
///
/// Templates are defined in template folder
var docs = function (routes) {
    handlebars_1.default.registerHelper('parseJoi', function (context, options) {
        var stack = context.filter(function (i) { return i.schema; });
        if (stack.length === 0)
            return '';
        return options.fn(stack[0].schema);
    });
    handlebars_1.default.registerHelper('isNotEmpty', function (context, options) {
        if (Array.isArray(context) && context.length > 0) {
            var uniq = context.filter(unique);
            if (uniq.length > 1 || uniq[0] !== '') {
                return options.fn(context);
            }
        }
    });
    handlebars_1.default.registerHelper('joiOrNot', function (conditional, options) {
        if (conditional && conditional.isJoi) {
            return options.fn(conditional);
        }
        else {
            return options.inverse(conditional);
        }
    });
    handlebars_1.default.registerHelper('describe', function (context, options) {
        if (context && context.isJoi)
            context = joiParser_1.default(context);
        return options.fn(new handlebars_1.default.SafeString(context));
    });
    handlebars_1.default.registerHelper('showit', function (context) {
        console.log('::SHOW::::::::', context);
        return '';
    });
    var partials = ['header', 'style', 'script', 'footer', 'validations'];
    partials.forEach(function (partial) {
        handlebars_1.default.registerPartial(partial, fs_1.default.readFileSync(path_1.default.join(__dirname, "../template/" + partial + ".handlebars"), 'utf8'));
    });
    var template = handlebars_1.default.compile(fs_1.default.readFileSync(path_1.default.join(__dirname, '../template/list.handlebars'), 'utf8'));
    return template({ routes: routes });
};
/// ### KoaAPIDocs
/// This is Koa middleware to add Joi documentation on top of the routes.
/// It needs to get list of routes and Joi validation schema should be on it's middleware as a property `valiationMiddleware.schema = JoiSchema`
///
/// There are few parameters you can configure `app.user(routes, {url: string, hideAt: array, enabled: boolean} )`
/// `url`: By default the documentation will be available on `/docs` url
/// `hideAt`: By default if process.env.NODE_ENV is one of these ['production'], the middleware will bypassed
/// `enabled`: by default the value is true and it is enabled.
var KoaAPIDocs = function (routes, configs) {
    if (configs === void 0) { configs = {}; }
    var defaults = {
        url: '/docs',
        hideAt: ['production'],
        enabled: true
    };
    configs = Object.assign(defaults, configs);
    var error = null;
    try {
        var isSingleRouter = isRouter(routes);
        var isArray = Array.isArray(routes);
        if (isArray) {
            routes.forEach(function (route) {
                if (!isRouter(route))
                    throw 'You should pass a koa router or array of koa routers';
            });
        }
        if (!isSingleRouter && !isArray)
            throw 'You should pass a koa router or array of koa routers';
        routes = isSingleRouter ? [routes] : routes.flat();
    }
    catch (err) {
        error = err;
    }
    return function (ctx, next) {
        if (!configs.enabled || configs.hideAt.indexOf(process.env.NODE_ENV) > -1 || ctx.url !== configs.url) {
            return next();
        }
        ctx.body = error ? "error: " + error.message : docs(routes);
    };
};
exports.default = KoaAPIDocs;
