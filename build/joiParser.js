"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var capitalizeFirstLetter = function capitalizeFirstLetter(string) {
  return string[0].toUpperCase() + string.slice(1);
};

var explainRule = function explainRule(name, arg) {
  if (name === 'regex') return arg.pattern;
  return JSON.stringify(arg);
};

var JoiParser = function JoiParser(schema) {
  var describeSchema = _joi["default"].describe(schema);

  var flags = {
    allowOnly: 'Value must exist within the valid values list',
    allowUnknown: 'Unknown keys/items are allowed but ignored',
    stripUnknown: 'Unknown keys/items will be removed'
  };
  var flagsToIgnore = ['presence', 'insensitive'];
  var output = '';

  var print = function print(data) {
    output += '<div class="schema">';

    if (data.type === 'alternatives') {
      output += '<div class="alternatives"><strong>One of the following:</strong><div>';
      data.alternatives.forEach(function (alternative) {
        output += '<div>';
        print(alternative);
        output += '</div>';
      });
      output += '</div></div>';
    } else {
      var insensitive = data.flags && data.flags.insensitive === true ? '<strong class="required box">insensitive</strong>' : '';
      var presence = data.flags && data.flags.presence ? data.flags.presence : '';
      var className = presence === 'required' ? 'required' : 'presence';

      if (presence !== '') {
        presence = "<strong class='".concat(className, " box'>").concat(data.flags.presence, "</strong>");
      }

      output += "<div class='type'><strong class='box'>".concat(data.type, "</strong>").concat(presence).concat(insensitive, "</div>");
    }

    if (data.rules) {
      output += '<div class="rules"><div class="rules-list">';
      data.rules.forEach(function (rule) {
        if (rule.arg === undefined) {
          output += "<div class=\"box rule\">is ".concat(rule.name, "</div>");
        } else {
          output += "<div class=\"box rule\">".concat(rule.name, ": ").concat(explainRule(rule.name, rule.arg), "</div>");
        }
      });
      output += '</div></div>';
    }

    if (data.items) {
      data.items.forEach(function (item) {
        output += '<div class="items"><div class="title box">items:</div><div class="items-list">';
        print(item);
        output += '</div></div>';
      });
    }

    var notes = data.notes ? data.notes.map(function (item) {
      return "<li>".concat(item, "</li>");
    }) : [];

    if (data.flags) {
      Object.keys(data.flags).forEach(function (flag) {
        if (flagsToIgnore.indexOf(flag) > -1) return;
        var filteredFlag = flags[flag] || "".concat(capitalizeFirstLetter(flag), ": ").concat(data.flags[flag]);
        notes.push("<li>".concat(filteredFlag, "</li>"));
      });
    }

    if (notes.length > 0) {
      output += "<div class=\"flags\"><ul>".concat(notes.join(), "</ul></div>");
    }

    if (data.valids) {
      data.valids = data.valids.filter(function (item) {
        return item != null;
      });

      if (data.valids.length > 0) {
        output += "<div class='valids'><strong class='box title'>valid values</strong>".concat(data.valids.map(function (i) {
          return "<div class='box term'>".concat(i, "</div>");
        }).join(''), "</div>");
      }
    }

    if (data.invalids) {
      data.invalids = data.invalids.filter(function (item) {
        return item != null;
      });

      if (data.invalids.length > 0) {
        output += "<div class='invalids'><strong class='box title'>invalid values</strong>".concat(data.invalids.map(function (i) {
          return "<div class='box term'>".concat(i || 'EMPTY_STRING', "</div>");
        }).join(''), "</div>");
      }
    }

    if (data.children) {
      output += '<div class="children-list"><div class="children-schema">';
      Object.keys(data.children).forEach(function (child) {
        output += "<div class='item'><strong class='key box'>".concat(child, "</strong><div class='validations'>");
        print(data.children[child]);
        output += '</div></div>';
      });
      output += '</div></div>';
    }

    output += '</div>';
  };

  print(describeSchema);
  return output;
};

var _default = JoiParser;
exports["default"] = _default;