import Joi from 'joi'

const capitalizeFirstLetter = (string: string) => string[0].toUpperCase() + string.slice(1);

const explainRule = (name, arg) => {
  if (name === 'regex') return arg.pattern
  return JSON.stringify(arg)
}

const JoiParser = (schema: any) => {
  const describeSchema = Joi.describe(schema);

  const flags = {
    allowOnly: 'Value must exist within the valid values list',
    allowUnknown: 'Unknown keys/items are allowed but ignored',
    stripUnknown: 'Unknown keys/items will be removed'
  }

  const flagsToIgnore = [
    'presence',
    'insensitive'
  ]

  let output = ''

  const print = (data: any) => {
    output += '<div class="schema">'

    if (data.type === 'alternatives') {
      output += '<div class="alternatives"><strong>One of the following:</strong><div>'
      data.alternatives.forEach((alternative: any) => {
        output += '<div>'
        print(alternative)
        output += '</div>'
      })
      output += '</div></div>'
    } else {
      let insensitive: string = data.flags && data.flags.insensitive === true
        ? '<strong class="required box">insensitive</strong>'
        : ''
      let presence: string = data.flags && data.flags.presence ? data.flags.presence : ''
      const className = presence === 'required' ? 'required' : 'presence'
      if (presence !== '') {
        presence = `<strong class='${className} box'>${data.flags.presence}</strong>`
      }
      output += `<div class='type'><strong class='box'>${data.type}</strong>${presence}${insensitive}</div>`
    }

    if (data.rules) {
      output += '<div class="rules"><div class="rules-list">'
      data.rules.forEach((rule: any) => {
        if (rule.arg === undefined) {
          output += `<div class="box rule">is ${rule.name}</div>`
        } else {
          output += `<div class="box rule">${rule.name}: ${explainRule(rule.name, rule.arg)}</div>`
        }
      })
      output += '</div></div>'
    }

    if (data.items) {
      data.items.forEach((item: any) => {
        output += '<div class="items"><div class="title box">items:</div><div class="items-list">'
        print(item)
        output += '</div></div>'
      })
    }

    let notes = data.notes ? data.notes.map((item: any) => `<li>${item}</li>`) : []
    if (data.flags) {
      Object.keys(data.flags).forEach((flag: string) => {
        if (flagsToIgnore.indexOf(flag) > -1) return
        const filteredFlag = flags[flag] || `${capitalizeFirstLetter(flag)}: ${data.flags[flag]}`
        notes.push(`<li>${filteredFlag}</li>`)
      })
    }
    if (notes.length > 0) {
      output += `<div class="flags"><ul>${notes.join()}</ul></div>`
    }

    if (data.valids) {
      data.valids = data.valids.filter((item: any) => item != null)
      if (data.valids.length > 0) {
        output += `<div class='valids'><strong class='box title'>valid values</strong>${data.valids
          .map((i) => `<div class='box term'>${i}</div>`)
          .join('')}</div>`
      }
    }


    if (data.invalids) {
      data.invalids = data.invalids.filter((item) => item != null)
      if (data.invalids.length > 0) {
        output += `<div class='invalids'><strong class='box title'>invalid values</strong>${data.invalids
          .map((i) => `<div class='box term'>${i || 'EMPTY_STRING'}</div>`)
          .join('')}</div>`
      }
    }

    if (data.children) {
      output += '<div class="children-list"><div class="children-schema">';

      Object.keys(data.children).forEach(child => {
        output += `<div class='item'><strong class='key box'>${child}</strong><div class='validations'>`
        print(data.children[child])
        output += '</div></div>'
      })

      output += '</div></div>'
    }

    output += '</div>'
  }

  print(describeSchema)

  return output
}

export default JoiParser