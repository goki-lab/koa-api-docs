"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
require("mocha");
var index_1 = __importDefault(require("../src/index"));
var testSamples = [
    { input: 'abc', expectedResult: 'abc', description: 'should return pristine value when receiving "abc"' },
    { input: 'abc1234567', expectedResult: 'abc1234567', description: 'should return pristine value when receiving "abc1234567"' },
    { input: 'abcdefghij', expectedResult: 'abcdefghij', description: 'should return pristine value when receiving "abcdefghij"' },
    { input: '1234567890', expectedResult: '(123) 456-7890', description: 'should return (123) 456-7890' },
    { input: '5431260987', expectedResult: '(543) 126-0987', description: 'should return (543) 126-0987' },
];
describe('Array', function () {
    testSamples.forEach(function (sample) {
        it(sample.description, function () {
            chai_1.expect(index_1.default(sample.input)).equal(sample.expectedResult);
        });
    });
});
