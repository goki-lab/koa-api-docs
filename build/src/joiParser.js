"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var joi_1 = __importDefault(require("joi"));
var capitalizeFirstLetter = function (string) { return string[0].toUpperCase() + string.slice(1); };
var explainRule = function (name, arg) {
    if (name === 'regex')
        return arg.pattern;
    return JSON.stringify(arg);
};
var JoiParser = function (schema) {
    var describeSchema = joi_1.default.describe(schema);
    var flags = {
        allowOnly: 'Value must exist within the valid values list',
        allowUnknown: 'Unknown keys/items are allowed but ignored',
        stripUnknown: 'Unknown keys/items will be removed'
    };
    var flagsToIgnore = [
        'presence',
        'insensitive'
    ];
    var output = '';
    var print = function (data) {
        output += '<div class="schema">';
        if (data.type === 'alternatives') {
            output += '<div class="alternatives"><strong>One of the following:</strong><div>';
            data.alternatives.forEach(function (alternative) {
                output += '<div>';
                print(alternative);
                output += '</div>';
            });
            output += '</div></div>';
        }
        else {
            var insensitive = data.flags && data.flags.insensitive === true
                ? '<strong class="required box">insensitive</strong>'
                : '';
            var presence = data.flags && data.flags.presence ? data.flags.presence : '';
            var className = presence === 'required' ? 'required' : 'presence';
            if (presence !== '') {
                presence = "<strong class='" + className + " box'>" + data.flags.presence + "</strong>";
            }
            output += "<div class='type'><strong class='box'>" + data.type + "</strong>" + presence + insensitive + "</div>";
        }
        if (data.rules) {
            output += '<div class="rules"><div class="rules-list">';
            data.rules.forEach(function (rule) {
                console.log(rule);
                if (rule.arg === undefined) {
                    output += "<div class=\"box rule\">is " + rule.name + "</div>";
                }
                else {
                    output += "<div class=\"box rule\">" + rule.name + ": " + explainRule(rule.name, rule.arg) + "</div>";
                }
            });
            output += '</div></div>';
        }
        if (data.flags) {
            output += '<div class="flags"><ul>';
            Object.keys(data.flags).forEach(function (flag) {
                if (flagsToIgnore.indexOf(flag) > -1)
                    return;
                var filteredFlag = flags[flag] || capitalizeFirstLetter(flag) + ": " + data.flags[flag];
                output += "<li>" + filteredFlag + "</li>";
            });
            output += '</ul></div>';
        }
        if (data.valids) {
            data.valids = data.valids.filter(function (item) { return item != null; });
            if (data.valids.length > 0) {
                output += "<div class='valids'><strong class='box title'>valid values</strong>" + data.valids
                    .map(function (i) { return "<div class='box term'>" + i + "</div>"; })
                    .join('') + "</div>";
            }
        }
        // if (data.invalids) {
        //   data.invalids = data.invalids.filter((item) => item != null)
        //   if (data.invalids.length > 0) {
        //     output += `<div class='invalids'><strong class='box title'>invalid values</strong>${data.invalids
        //       .map((i) => `<div class='box term'>${i}</div>`)
        //       .join('')}</div>`
        //   }
        // }
        if (data.children) {
            output += '<div class="children-list"><div class="children-schema">';
            Object.keys(data.children).forEach(function (child) {
                output += "<div class='item'><strong class='key box'>" + child + "</strong><div class='validations'>";
                print(data.children[child]);
                output += '</div></div>';
            });
            output += '</div></div>';
        }
        output += '</div>';
    };
    print(describeSchema);
    return output;
};
exports.default = JoiParser;
